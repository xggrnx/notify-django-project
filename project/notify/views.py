import datetime

from django.shortcuts import render, redirect
from django.views import View

from project.notify.models import Notify, NotifyData, NotifyUser
from project.users.admin import User

from project.notify.tasks import send_notify


class NotifyView(View):

    def get(self, request, *args, **kwargs):
        ctx = {}
        ctx['users'] = User.objects.all()
        ctx['channels'] = Notify.CHANNEL_CHOICES
        return render(request=request, template_name='pages/home.html', context=ctx)

    def post(self, request, *args, **kwargs):
        #@TODO Я знаю чтол это всё  нужно в форму завернуть,
        # Сделать валидацию и проверку ошибок
        # Но сейчас я уже уже 10й час за компом и мне чутка лень
        data = request.POST
        user_ids = [int(id) for id in data.getlist('users')]
        channels = [int(ch) for ch in data.getlist('channels')]
        if data.get('send_now'):
            date = datetime.datetime.now()
        else:
            date = data['datetime'] or datetime.datetime.now() + datetime.timedelta(minutes=10) # noqa
        notify_data = NotifyData.objects.create(
            template=data['template'],
            text=data['text']
        )
        notify = Notify.objects.create(
            data=notify_data,
            name=data['text'],
            send_at=date,
            channels=channels
        )
        for user_id in user_ids:
            user = User.objects.get(pk=int(user_id))
            NotifyUser.objects.create(user=user, notify=notify)

        send_notify.delay(notify.pk)
        return redirect('/')
