from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class NotifyConfig(AppConfig):
    name = "project.notify"
    verbose_name = _("Notify")

    def ready(self):
        try:
            import project.notify.tasks  # noqa F401
        except ImportError:
            pass

