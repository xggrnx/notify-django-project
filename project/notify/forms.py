from django import forms
from project.notify.models import Notify
from project.utils.fields import ArrayFieldSelectMultiple


class NotifyForm(forms.ModelForm):
    class Meta:
        model = Notify
        fields = [
            'name',
            'channels',
            'data',
            'send_at',
            'users',
        ]
        widgets = {
            "channels": ArrayFieldSelectMultiple(
                choices=Notify.CHANNEL_CHOICES, attrs={'class': 'chosen'}),
        }
