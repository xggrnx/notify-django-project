import os

from django.conf import settings as conf_settings

from jinja2 import Template

class NotifyChannel:
    @classmethod
    def send(cls, path: str, template: str, data: str):
        try:
            file_path = os.path.join(
                conf_settings.ROOT_DIR,
                'notify_docs',
                path
            )
            Template(template).stream(data=data).dump(file_path)
        except:
            #@TODO log exception
            pass


class XmlChannel(NotifyChannel):
    pass


class TextChannel(NotifyChannel):
    pass
