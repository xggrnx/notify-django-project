from django import forms
from django.contrib import admin

from project.notify.forms import NotifyForm
from project.notify.models import Notify, NotifyData, NotifyUser


@admin.register(Notify)
class NotifyModelAdmin(admin.ModelAdmin):
    form = NotifyForm


admin.site.register(NotifyData)
admin.site.register(NotifyUser)
