from factory import Faker

from factory.django import DjangoModelFactory

from project.notify.models import Notify, NotifyData

from datetime import datetime, timedelta

import random

choices = [k for k, _ in Notify.CHANNEL_CHOICES]


class NotifyDataFactory(DjangoModelFactory):
    class Meta:
        model = NotifyData
    template = "Hello text here {data}"
    text = Faker("lexify")


class NotifyFactory(DjangoModelFactory):
    class Meta:
        model = Notify

    created_at = datetime.now() - timedelta(days=1)
    sent_at = datetime.now() - timedelta(minutes=1)
    channel = random.choice(choices)
    data = NotifyDataFactory()
    sent = False
