from datetime import datetime

from typing import List

from config import celery_app

from project.notify.models import Notify, NotifyUser


@celery_app.task()
def send_notify(notify_id=None):
    """Get Notify and send to celery task"""
    if notify_id:
        query: List[NotifyUser] = NotifyUser.objects.filter(
            notify__pk=notify_id,
        )
    else:
        query: List[NotifyUser] = NotifyUser.objects.filter(
            notify__send_at__lte=datetime.now(),
            sent=False
        )
    _channels = {k: v for k, v in Notify.CHANNEL_CHOICES}
    for q in query:
        notify_template: str = q.notify.data.template
        notify_text: str = q.notify.data.text
        for channel in q.notify.channels:
            channel_name: str = _channels[channel]
            user_channel = q.user.channels.get(channel_name)
            if user_channel:
                user_channel = f"{q.id} {user_channel}"
                proceed_notify.delay(
                    q.id,
                    channel,
                    user_channel,
                    notify_template,
                    notify_text
                )

@celery_app.task()
def proceed_notify(
    id: int,
    channel: int,
    user_channel: str,
    template: str,
    text: str
):
    """Send notify data to service"""
    try:
        service = Notify.CHANNEL_SERVICES[channel]
        service.send(user_channel, template, text)
    except Exception: # @TODO Service send exception here
        pass
    finally:
        obj = NotifyUser.objects.get(pk=id)
        obj.sent = True
        obj.save()
