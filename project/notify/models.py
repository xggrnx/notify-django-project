from django.db import models
from django.contrib.postgres.fields import ArrayField

from django.utils.translation import gettext_lazy as _

from project.notify.services import XmlChannel, TextChannel
from project.users.models import User



class NotifyData(models.Model):
    """Notification Content"""
    template = models.CharField(_("Template"), max_length=255)
    text = models.TextField()

    def __str__(self):
        return f"{self.pk } Notify data: {self.text}"

class Notify(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    users = models.ManyToManyField(User, through='NotifyUser')

    created_at = models.DateTimeField(auto_now_add=True)
    send_at = models.DateTimeField(
        _("Notify Sent"),
        blank=True,
        null=True,
    )
    channels = ArrayField(
        models.IntegerField(),
        default=list,
        blank=True,
    )
    data = models.OneToOneField(
        NotifyData,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    CHANNEL_CHOICES = (
        (1, 'XML'),
        (2, 'TXT'),
    )
    CHANNEL_SERVICES = {
        1: XmlChannel,
        2: TextChannel,
    }

    def __str__(self):
        return f"{self.pk} {self.name}"


class NotifyUser(models.Model):
    """3NF model"""
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notify = models.ForeignKey(Notify, on_delete=models.CASCADE)
    sent = models.BooleanField(default=False)  # Notify state

    def __str__(self):
        return f"{self.pk} {self.notify.name} User: {self.user.pk} {self.sent}"
