from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    """Default user for notify_app."""

    #: First and last name do not cover name patterns around the globe
    name = models.CharField(_("Name of User"), blank=True, max_length=255)
    xml_address = models.CharField(_('Xml'), blank=True, max_length=255)
    txt_address = models.CharField(_('Txt'), blank=True, max_length=255)

    @property
    def channels(self):
        ch = dict()
        if self.xml_address:
            ch['XML'] = f"{self.xml_address}.xml"
        if self.txt_address:
            ch['TXT'] = f"{self.txt_address}.txt"
        return ch

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})


